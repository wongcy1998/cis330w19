//
// Returns n factorial
//
int
factorial(int n)
{
  return n > 0 ? factorial(n - 1) : 1;
}
