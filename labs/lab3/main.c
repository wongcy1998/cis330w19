#include <stdio.h>
#include "power.h"

int
main(int argc, char **argv)
{
  printf("4 to the 5 is %f\n", power(4.0, 5.0));

  return 0;
}
