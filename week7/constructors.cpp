#include <iostream>
#include <string>
#include <vector>

using namespace std;
class A {
  public:
    A(int len=4) { cout << "Default\n"; vals.reserve(len);} // default
    A(A& existing) { //copy (shallow)
      cout << "Copy\n";
      this->vals = existing.vals;
    } 
    A(A&& existing) { //move (deep)
	  cout << "Move\n";
	  vals = existing.vals;
    } 

    A(string CheerfulMessage) {  // just an custom constructor
	  cout << "Custom\n";
	  vals.reserve(CheerfulMessage.size()); // why? who knows...
	  cout << CheerfulMessage << endl; 
    }
    ~A() { vals.clear(); }

    friend void print( A& a) { 
      for (auto it = a.vals.begin(); it != a.vals.end(); it++) 
        cout << *it << ","; 
      cout << endl; 
    }

  private:
    vector<int> vals;
};

int main() {
    A first; 
    A second = first;
    A third(second);
}
