/*
 * farm.cpp
 *
 *  Created on: Jan 28, 2014
 *      Author: norris
 */

#include "allanimals.hpp"
#include "farmer.hpp"

using namespace world;

int main() {

	Animal * animal = nullptr;

	Sheep * sheep = new Sheep();
        
	animal = sheep; 

        animal->eat();

	Wolf wolf;

	animal = &wolf; 

        animal->eat();

	wolf.hunt( sheep );

        return 0;
}
