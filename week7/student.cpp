#include <utility>
#include <string>
using namespace std;
class Student {
    private:
	std::string first;
	std::string last;
	int id;

    public:
	// Constructor #1
	Student(const string& f, const string& l="", int i = 0) 
	    : first(f), last(l), id(i) {
		// Question? How many copy assignments for std::string?
	    }

	// Constructor #2
	Student(const char* f, const char* l = "", int i = 0) 
	    : first(f), last(l), id(i) {
	    }

	// Constructor #3
	Student(const std::string& f, const char* l = "", int i = 0) 
	    : first(f), last(l), id(i) {
	    }   

	// Constructor #4
	Student(const char* f, const std::string& l = "", int i = 0) 
	    : first(f), last(l), id(i) {
	    }   

};

int main() {
    Student s1{"Joe","Fix",42};   // C1: 4 expensive string copies (mallocs!) with 
    // C2: 2 mallocs (2 creates)

    std::string s = "Joe";
    Student s2{s, "Fix", 42};	// C1: 3 mallocs (1 cp + 2 creates)

    // So we overload for all 4 combinations of params to get just 2 mallocs!
    //

    // What about MOVE SEMANTICS
    Student s3{std::move(s), "Fix", 42}; // still 2mallocs (1cp + 1 create) 
    //  -- no move support in constructor of string class!

}

