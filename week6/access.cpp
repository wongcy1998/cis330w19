#include <iostream>
using namespace std;

class base
{
public:
  int x;
  void foo() {cout << "base::foo() x=" << this->x << endl;}
protected:
  int y;
private:
  int z;
};

class publicDerived: public base
{
  // x is public
  // y is protected
  // z is not accessible from publicDerived
public:
  void foo() {cout << "publicDerived::foo() x=" << x << endl; }
};

class protectedDerived: protected base
{
  // x is protected
  // y is protected
  // z is not accessible from protectedDerived
public:
  void foo() {cout << "protectedDerived::foo() x=" << x << endl; }
};

class privateDerived: private base
{
  // x is private
  // y is private
  // z is not accessible from privateDerived
public:
  void foo() {cout << "privateDerived::foo() x=" << x << endl; }
  
};

class privateDerivedChild : privateDerived
{
  // x, y, z not accessible
public:
  // cannot access x, which is private in privateDerived
  //void bar() {cout << "privateDerivedChild::bar() x=" << x << endl; } 
};

int main() {
  base b; b.foo();
  publicDerived pud; pud.foo();
  protectedDerived prod; prod.foo();
  privateDerived prid; prid.foo();
  
  privateDerivedChild pridc; //pridc.bar();
}

