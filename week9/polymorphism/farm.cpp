/*
 * farm.cpp
 *
 *  Created on: Jan 28, 2014
 *      Author: norris
 */

#include <vector>
#include <cstdlib>
#include <iostream>
#include "allanimals.hpp"
#include "farmer.hpp"

using namespace world;

void animalThings(Animal *predator, Animal *animal) {
	animal->legCount();
	animal->eat();
	animal->drink();

	Wolf *wolf = dynamic_cast<Wolf*>(predator);
	if (wolf != nullptr)
		wolf->hunt(animal);
}

Quadruped *getSheep() {
	return new Sheep();
}

void huntingHelper(Wolf* wolf,Animal* animal) {
	wolf->hunt(animal);
}

int main() {

	using namespace std;

	Quadruped *animal = getSheep();
	Animal *wolf = new Wolf();

	animalThings(wolf,wolf);

	//huntingHelper(wolf,animal);
    huntingHelper(static_cast<Wolf*>(wolf),animal);
	return 0;
}
