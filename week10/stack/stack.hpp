/* Filename:  stack.hpp

   Programmer:  Br. David Carlson

   Date:  March 28, 1998

   This is the header file to provide the abstract base class called
   StackBaseClass.
*/

#include "itemtype.hpp"


class StackBaseClass
   {
   public:
      virtual void Push(const ItemType & Item) = 0;
      virtual void Pop(ItemType & Item) = 0;
      virtual bool Empty(void) const = 0;
   };

// The = 0; means there is no code; these are "pure virtual" functions.


