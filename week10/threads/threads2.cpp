#include <iostream>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>    // mutex
#include <chrono>

class MultiCounter {
public: 
     MultiCounter() : sum(0) {}
     void computeSum(int *a, int n);
     int  getSum() { return sum; }
private:
     int64_t sum;   // the shared variable
};

void 
MultiCounter::computeSum(int* a, int n) { 

     for (int i = 0; i < n; ++i) {
        sum += a[i];
     }
     //std::cout << "Thread " << std::this_thread::get_id() << ": counter=" << sum << std::endl;
}

int main()
{
    MultiCounter counter;
    int const n = 1000000;

    int a[n]; 
    for (int i = 0; i < n; i++) a[i] = 1;

    auto start = std::chrono::steady_clock::now(); // timer

    // Create a vector of threads, each of which executes 'computeSum'
    std::vector<std::unique_ptr<std::thread> > threads;
    for (int i = 0; i < 10; i++)  
        threads.push_back(std::unique_ptr<std::thread>
              (new std::thread(&MultiCounter::computeSum, &counter, a, n)));

    for (int i = 0; i < 10; i++)  
        threads[i]->join();    // wait for each thread to finish

    auto elapsed = std::chrono::steady_clock::now() - start; // timer
    std::cout << "Final result: " << counter.getSum() << std::endl;
    std::cout << "Elapsed time: " << std::chrono::duration <double, std::milli> (elapsed).count() << std::endl;
}
