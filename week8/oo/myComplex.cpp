#include <iostream>
using namespace std;

class MyComplex
{
private:
	double real, imag;
public:
	MyComplex(){
		real = 0; 
		imag = 0;
	}

	MyComplex(double r, double i) {
		real = r;
		imag = i;
	}

	double getReal() const {
		return real;
	}

	double getImag() const {
		return imag;
	}

	MyComplex & operator=(const MyComplex &);
	const MyComplex operator+(const MyComplex & );
	MyComplex & operator++(void);
	MyComplex  operator++(int);

	/*friend const 
		MyComplex operator+(const MyComplex&, const MyComplex&); */

	friend ostream& operator<<(ostream& os, const MyComplex& c);

    // note: no const for the second parameter
	friend istream& operator>>(istream& is, MyComplex& c);
};

MyComplex & MyComplex::operator=(const MyComplex& c) {
	real = c.real;
	imag = c.imag;
	return *this;
}

const MyComplex MyComplex::operator+(const MyComplex& c) {
	MyComplex temp;
	temp.real = this->real + c.real;
	temp.imag = this->imag + c.imag;
	return temp;
}

//pre-increment
MyComplex & MyComplex::operator++() {
	real++;
	imag++;
	return *this;
}

//post-increment
MyComplex MyComplex::operator++(int) {
	    MyComplex temp = *this;
	    real++;
	    imag++;
	    return temp;
}

/* This is not a member function of MyComplex class */
/*
const MyComplex operator+(const MyComplex& c1, const MyComplex& c2) {
	    MyComplex temp;
	    temp.real = c1.real + c2.real;
	    temp.imag = c1.imag + c2.imag;
	    return temp;
}*/


ostream& operator<<(ostream &os, const MyComplex& c) {
	os << c.real << '+' << c.imag << 'i';
	return os;
}


istream& operator>>(istream &is, MyComplex& c) {
	is >> c.real >> c.imag;
	return is;
}

int main()
{
#if 1
	MyComplex c1(5,10);
	cout << "c1 = " << c1.getReal() << "+" << c1.getImag() << "i" << endl;

	cout << "Using overloaded ostream(<<) " << endl;
	cout << "c1 = " << c1 << endl;

	MyComplex c2;
	cout << "Enter two numbers: " << endl;
	cin >> c2;
	cout << "Using overloaded istream(>>) " << endl; 
	cout << "Input complex is = " << c2;
#else
	MyComplex c1(5,10);
	MyComplex c2(50,100);
	cout << "c1= " << c1 << endl;
	cout << "c2= " << c2 << endl;
	c2 = c1;
	cout << "assign c1 to c2:" << endl; 
	cout << "c2= " << c2 << endl;
	cout << endl;

	MyComplex c3(10,100);
	MyComplex c4(20,200);
	cout << "c3= " << c3 << endl;
	cout << "c4= " << c4 << endl;;
	MyComplex c5 = c3 + c4;
	cout << "c5= " << c5 << endl;

#endif

	return 0;
}
