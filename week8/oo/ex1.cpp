#include <iostream>

using namespace std;
class A
{
public:
	A(){}
	A(const A& a) {this->data = a.data;}
	explicit A(int n):data(n) {}
	int data;
	A operator+(A&);
	A operator+(int);
	A operator++(int);
	A& operator++();
};

// + overloading
A A::operator+(A& obj)
{
	A *tmp = new A(*this);
	tmp->data = this->data + obj.data;
	return *tmp;
}

A A::operator+(int i)
{
	A *tmp = new A(*this);
	tmp->data = this->data + i;
	return *tmp;
}

// post increment (x++) overloading
// returns original value, and then increment the value
// copy needed
// return a locally created object. 
// Note that it's not returning a reference since it's a temporary obj.
A A::operator++(int)
{
	A tmp = *this;
	this->data = this->data + 1;
	return tmp;
}

// pre increment (++x) overloading
// returns incremented tvalue
// no copy necessary
// pre increment (++x) overloading
A& A::operator++()
{
	this->data = (this->data)+1;
	return *this;
}

int main()
{
	A obj1(10);
	A obj2(20);
	A obj3 = obj1 + 4;  // obj3.data = 10 + 20 = 30
	A obj4 = obj1++;       // obj4.data = 10, obj1.data = 11 
	A obj5 = ++obj2;       // obj5.data = 21, obj2.data = 21
    A obj6 = obj1.operator++();

	cout << obj1.data << " " << obj6.data << endl;
	cout << obj3.data << endl;
	cout << obj4.data << endl;
	cout << obj5.data << endl;

	return 0;
}
