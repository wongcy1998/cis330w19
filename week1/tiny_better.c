#include <stdio.h>   /* Needed for printf and getchar */
#include <stdlib.h>
#include <string.h>
int main() 
{
    const int maxBufferSize = 5;

    char *array_input = (char *) malloc(maxBufferSize * sizeof(char));

    printf("Are you having a nice day? (y/n)? ");

    fgets(array_input, maxBufferSize, stdin);

    if (strcmp(array_input, "yes\n") == 0 || strcmp(array_input, "y\n") == 0) //for strcmp
	   //returns value < 0 if str1 < str2
	   //returns 0 < value if str2 is less than str1
	   //if return == 0, then str1 == str2 
    {
        printf("That's wonderful, so am I!\n");
    } 
    
    else 
    {
        printf("That sucks, just try again tomorrow.\n");
    }
    free(array_input);
    return 0;
}
