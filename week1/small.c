#include <stdio.h>
#include <stdlib.h>

int main() {
    //char response[50];
    char *response = NULL;
    int size = 0;
    printf("How much room do you need to tell me how you feel today? ");
    scanf("%d", &size);

    response = (char *) malloc(size * sizeof(char));
    printf("How do you feel today? ");
    // Something to think about: what happens when the input contains
    // spaces or is longer than size?
    scanf("%s",response);
    printf("You said: %s -- how wonderful!\n", response);
    return 0;
}
