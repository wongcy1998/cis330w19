#include <stdlib.h> // for malloc
#include <string.h> // for strcpy and strlen

#include "puppy.h"

void init(const char someName[], Name *animal) {
     *animal = malloc( strlen(someName) );
     strcpy(*animal,someName);
}

void release(Name *animal) {
     free(*animal);
}
