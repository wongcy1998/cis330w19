// File: animals.cpp
#include <iostream>
#include <string> 

using namespace std;
#include "animals.hpp"

int main() {
  string usercolor;
  
  cin >> usercolor;
  
  animals::Sheep sheep1(usercolor);
  
  cout << "A colorful sheep is born -- " << sheep1.getColor().getName() << endl;
  
}
