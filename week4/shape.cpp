// File: shape.cpp
#include "shape.hpp"

namespace geometry {
  
  Shape::Shape() : color(1), name("shape") {
  }
  
  Rectangle::Rectangle(double length, double width) : Shape() {
    this->length = length;
    this->width = width;
    this->name = "rectangle";
  }
  
  double Rectangle::getArea() {
    return this->length * this->width;
  }
  
  // Default constructor
  Square::Square() : Rectangle(0,0) {
    this->name = "square";
  }
  
  Square::Square(double size) : Rectangle(size,size) {
    this->name = "square";
  }
  
  
}
